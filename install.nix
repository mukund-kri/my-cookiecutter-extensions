with import <nixpkgs> {};
with pkgs.python37Packages;


let
  my_cookiecutter_ext = buildPythonPackage rec {
    name = "my-cookiecutter-ext";
    version = "0.0.2";

    src = ./.;

    propagatedBuildInputs = [ jinja2 ];
  };
in
python37.withPackages (ps: [ my_cookiecutter_ext cookiecutter ])

  
