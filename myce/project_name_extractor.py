"""Jinja2 extensions."""

from jinja2.ext import Extension
		

class TargetExtractorExtension(Extension):

    def __init__(self, environment):
        super(TargetExtractorExtension, self).__init__(environment)

        def to_project_name(name):
            target = name.split('-')[1].capitalize()
            return target

        environment.filters['toProjectName'] = to_project_name
