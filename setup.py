import setuptools

requirements = [
    'jinja2>=2.7',
]


setuptools.setup(
    name="my-cookiecutter-extensions",
    version= "0.0.1",
    author="Mukund K",
    description="My personal cookiecutter extensions",
    packages=['myce'],

    python_requires=">=3.7",
    install_requires=requirements,
)
